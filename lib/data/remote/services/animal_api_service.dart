import 'package:animalsapp/data/remote/entity/animals_entity.dart';
import 'package:animalsapp/data/remote/services/request_api_service.dart';

class AnimalAPIService {
  static const animalPath = "/animals";

  AnimalAPIService._();

  static AnimalAPIService? _instance;

  static AnimalAPIService get instance {
    if (_instance == null) {
      _instance = AnimalAPIService._();
    }
    return _instance!;
  }

  Future<List<AnimalEntity>?> searchAnimals({required String search}) async {
    String url = "$animalPath?name=$search";
    final response = await RequestAPIService.instance.get(url: url);
    return List.from(response).map((e) => AnimalEntity.fromJson(e)).toList();
  }
}
