import 'dart:async';
import 'package:dio/dio.dart';

class RequestAPIService {
  RequestAPIService._();

  static RequestAPIService? _instance;

  static RequestAPIService get instance {
    if (_instance == null) {
      _instance = RequestAPIService._();
    }
    return _instance!;
  }

  final Dio dio = Dio();

  Future<dynamic> get({required String url}) async {
    Options options = Options();
    options = Options(
        headers: {'X-Api-Key': '7L4cjCiHbgLIlVRkEz1qAw==oEshGBcdaDDodOFq'});
    try {
      final response = await dio.get('https://api.api-ninjas.com/v1' + url,
          options: options);
      return response.data;
    } on DioError catch (dioError) {
      print(dioError);
    }
  }
}
