class AnimalEntity {
  String name;
  Taxonomy taxonomy;
  List<String> locations;
  Characteristics characteristics;
  AnimalEntity(
      {required this.name,
      required this.taxonomy,
      required this.locations,
      required this.characteristics});

  factory AnimalEntity.fromJson(Map<String, dynamic> json) {
    return AnimalEntity(
      name: json['name'],
      taxonomy: Taxonomy.fromJson(json['taxonomy']),
      locations: List.from(json['locations'] ?? []),
      characteristics: Characteristics.fromJson(json['characteristics']),
    );
  }
}

class Taxonomy {
  String? kingdom;
  String? phylum;
  String? classic;
  String? order;
  String? family;
  String? genus;
  String? scientificName;
  Taxonomy(
      {this.kingdom,
      this.phylum,
      this.classic,
      this.order,
      this.family,
      this.genus,
      this.scientificName});
  Map<String, String> toMapString() {
    return {
      "Kingdom": kingdom.toString(),
      "Phylum": phylum.toString(),
      "Class": classic.toString(),
      "Order": order.toString(),
      "Family": family.toString(),
      "Genus": genus.toString(),
      "Scientific name": scientificName.toString()
    };
  }

  factory Taxonomy.fromJson(Map<String, dynamic> json) {
    return Taxonomy(
      kingdom: json['kingdom'],
      phylum: json['phylum'],
      classic: json['classic'],
      order: json['order'],
      family: json['family'],
      genus: json['genus'],
      scientificName: json['scientific_name'],
    );
  }
}

class Characteristics {
  String? prey;
  String? nameOfYoung;
  String? groupBehavior;
  String? estimatedPopulationSize;
  String? biggestThreat;
  String? mostDistinctiveFeature;
  String? gestationPeriod;
  String? habitat;
  String? diet;
  String? averageLitterSize;
  String? lifestyle;
  String? commonName;
  String? numberOfSpecies;
  String? location;
  String? slogan;
  String? group;
  String? color;
  String? skinType;
  String? topSpeed;
  String? lifespan;
  String? weight;
  String? height;
  String? ageOfSexualMaturity;
  String? ageOfWeaning;
  Characteristics(
      {this.prey,
      this.nameOfYoung,
      this.groupBehavior,
      this.estimatedPopulationSize,
      this.biggestThreat,
      this.mostDistinctiveFeature,
      this.gestationPeriod,
      this.habitat,
      this.diet,
      this.averageLitterSize,
      this.lifestyle,
      this.commonName,
      this.numberOfSpecies,
      this.location,
      this.slogan,
      this.group,
      this.color,
      this.skinType,
      this.topSpeed,
      this.lifespan,
      this.weight,
      this.height,
      this.ageOfSexualMaturity,
      this.ageOfWeaning});

  Map<String, String> toMapString() {
    return {
      "Prey": prey.toString(),
      "Name of Young": nameOfYoung.toString(),
      "Group behavior": groupBehavior.toString(),
      "Estimated population size": estimatedPopulationSize.toString(),
      "Biggest threat": biggestThreat.toString(),
      "Most distinctive feature": mostDistinctiveFeature.toString(),
      "Gestation period": gestationPeriod.toString(),
      'Habitat': habitat.toString(),
      'Diet': diet.toString(),
      'Average litter size': averageLitterSize.toString(),
      'Lifestyle': lifestyle.toString(),
      'Common name': commonName.toString(),
      'Number of species': numberOfSpecies.toString(),
      'Location': location.toString(),
      'Slogan': slogan.toString(),
      'Group': group.toString(),
      'Color': color.toString(),
      'Skin type': skinType.toString(),
      'Top speed': topSpeed.toString(),
      'Lifespan': lifespan.toString(),
      'Weight': weight.toString(),
      'Height': height.toString(),
      'Age of sexual maturity': ageOfSexualMaturity.toString(),
      'Age of weaning': ageOfWeaning.toString(),
    };
  }

  factory Characteristics.fromJson(Map<String, dynamic> json) {
    return Characteristics(
      prey: json['prey'],
      nameOfYoung: json['name_of_yound'],
      groupBehavior: json['group_behavior'],
      estimatedPopulationSize: json['estimated_population_size'],
      biggestThreat: json['biggest_threat'],
      mostDistinctiveFeature: json['most_distinctive_feature'],
      gestationPeriod: json['gestation_period'],
      habitat: json['habitat'],
      diet: json['diet'],
      averageLitterSize: json['average_litter_size'],
      lifestyle: json['lifestyle'],
      commonName: json['common_name'],
      numberOfSpecies: json['number_of_species'],
      location: json['location'],
      slogan: json['slogan'],
      group: json['group'],
      color: json['color'],
      skinType: json['skin_type'],
      topSpeed: json['top_speed'],
      lifespan: json['lifespan'],
      weight: json['weight'],
      height: json['height'],
      ageOfSexualMaturity: json['age_of_sexual_maturity'],
      ageOfWeaning: json['age_of_weaning'],
    );
  }
}
