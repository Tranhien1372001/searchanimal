import 'package:animalsapp/data/remote/entity/animals_entity.dart';
import 'package:animalsapp/data/remote/services/animal_api_service.dart';

class AnimalRepository {
  static AnimalRepository? _instance;

  AnimalRepository._();

  static AnimalRepository get instance {
    if (_instance == null) {
      _instance = AnimalRepository._();
    }
    return _instance!;
  }

  List<AnimalEntity> _animals = [];

  List<AnimalEntity> get animals => _animals;

  Future<List<AnimalEntity>> searchanimals({required String search}) async {
    final animals =
        await AnimalAPIService.instance.searchAnimals(search: search);
    _animals.addAll(animals as List<AnimalEntity>);
    return _animals;
  }
}
