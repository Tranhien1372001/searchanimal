class AssetImages {
  AssetImages._();

  static const String iconWorld = "assets/icons/icons-world.png";
  static const String iconHeight = "assets/icons/icons-height.png";
  static const String iconScale = "assets/icons/icons-scale.png";
  static const String iconHeart = "assets/icons/icons-heart.png";
}
