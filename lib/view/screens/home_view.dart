import 'package:animalsapp/data/remote/entity/animals_entity.dart';
import 'package:animalsapp/view/assets/asset_images.dart';
import 'package:animalsapp/view/constants/ui_colors.dart';
import 'package:animalsapp/viewmodels/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late Size size;
  late HomeViewModel _homeViewModel;

  @override
  void initState() {
    super.initState();
    initViewModel();
  }

  void initViewModel() {
    _homeViewModel = HomeViewModel()..loadDataTemplate();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
        create: (context) => _homeViewModel,
        child: Scaffold(
          backgroundColor: UIColors.background,
          body: _buildBody(),
        ));
  }

  Container _buildBody() {
    return Container(
      height: size.height,
      width: size.width,
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(top: 80),
          height: size.height - 80,
          width: size.width,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50), topRight: Radius.circular(50))),
          child: Column(children: [
            _buildSearch(),
            _buildListItem(),
          ]),
        ),
      ),
    );
  }

  Container _buildListItem() {
    return Container(
      height: size.height - 200,
      child: SingleChildScrollView(
        child: Selector<HomeViewModel, List<AnimalEntity>>(
            selector: (_, viewModel) => viewModel.animalList,
            builder: (_, data, __) {
              if (data != null && data.length > 0) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: List.generate(
                      data.length,
                      (index) => InkWell(
                            child: _buildItem(data[index]),
                            onTap: () {
                              _homeViewModel.navigatorToDeatail(
                                  context, data[index]);
                            },
                          )),
                );
              }
              return Container();
            }),
      ),
    );
  }

  Container _buildItem(AnimalEntity animal) => Container(
        width: size.width,
        margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 12),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: UIColors.backgroundWhite,
            border: Border.all(color: UIColors.background.withOpacity(0.5)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: const Offset(1, 1),
                  blurRadius: 12)
            ]),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Row(children: [
            Expanded(
              flex: 6,
              child: Text(
                animal.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: UIColors.title,
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
            ),
            Expanded(
              flex: 4,
              child: Text(
                (animal.characteristics.group ?? "").toString(),
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.right,
                maxLines: 1,
                style: const TextStyle(fontWeight: FontWeight.w700),
              ),
            )
          ]),
          const Divider(),
          textInformationCustom(AssetImages.iconHeart,
              animal.characteristics.lifespan.toString()),
          textInformationCustom(
              AssetImages.iconHeight, animal.characteristics.height.toString()),
          textInformationCustom(
              AssetImages.iconScale, animal.characteristics.weight.toString()),
          textInformationCustom(
              AssetImages.iconWorld, animal.locations.join(', ')),
        ]),
      );

  Container textInformationCustom(String imagePath, String content) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Row(children: [
        Image.asset(
          imagePath,
          width: 25,
          height: 25,
        ),
        Container(
          width: (size.width - 120),
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            content != "null" ? content : "...",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ]),
    );
  }

  TextEditingController searchController = TextEditingController();
  Widget _buildSearch() {
    return Container(
      height: 50,
      width: size.width - 60,
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: UIColors.background.withOpacity(0.2),
          borderRadius: BorderRadius.circular(50),
          border: Border.all(color: UIColors.background)),
      child: Row(
        children: [
          Icon(
            Icons.search_rounded,
            color: UIColors.background,
          ),
          SizedBox(
            width: size.width - 130,
            child: TextField(
              decoration: const InputDecoration(
                hintText: "What do you want to search?",
                border: InputBorder.none,
              ),
              controller: searchController,
              onChanged: (value) {
                _homeViewModel.search(value);
              },
            ),
          ),
        ],
      ),
    );
  }
}
