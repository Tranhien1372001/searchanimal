import 'package:animalsapp/data/remote/entity/animals_entity.dart';
import 'package:animalsapp/view/constants/ui_colors.dart';
import 'package:flutter/material.dart';

class AnimalInformationView extends StatelessWidget {
  AnimalEntity animalEntity;
  AnimalInformationView({super.key, required this.animalEntity});

  late Size size;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: UIColors.backgroundWhite,
      appBar: AppBar(
          backgroundColor: UIColors.background, title: Text(animalEntity.name)),
      body: _buildBody(),
    );
  }

  Container _buildBody() {
    return Container(
      height: size.height,
      width: size.width,
      child: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const SizedBox(
            height: 10,
          ),
          boxInformationCustom({"Location": animalEntity.locations.join(', ')}),
          _buildTitle("Taxonomy"),
          boxInformationCustom(animalEntity.taxonomy.toMapString()),
          _buildTitle("Characteristics"),
          boxInformationCustom(animalEntity.characteristics.toMapString()),
        ]),
      ),
    );
  }

  Padding _buildTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: Text(
        title,
        style: TextStyle(
            color: UIColors.title, fontSize: 18, fontWeight: FontWeight.w700),
      ),
    );
  }

  Container boxInformationCustom(Map<String, String> data) {
    List<Widget> children = [];
    for (var entry in data.entries) {
      children.add(_buildItem(entry.key, entry.value));
    }
    return Container(
      width: size.width,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.black.withOpacity(0.2),
            offset: const Offset(1, 1),
            blurRadius: 4)
      ]),
      child: Column(
        children: children,
      ),
    );
  }

  Container _buildItem(String title, String content) {
    return Container(
      width: size.width,
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Text(
                title,
                style:
                    const TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              )),
          const SizedBox(width: 10),
          Expanded(
              flex: 7,
              child: Text(
                content == "null" ? "..." : content,
                style: const TextStyle(fontSize: 16),
              ))
        ],
      ),
    );
  }
}
