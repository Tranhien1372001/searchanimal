import 'package:animalsapp/view/screens/home_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeView(),
    );
  }
}

class CountScreen extends StatefulWidget {
  @override
  _CountScreenState createState() => _CountScreenState();
}

class _CountScreenState extends State<CountScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CounterProvider(),
      builder: (BuildContext context, _) {
        final counterProvider =
            Provider.of<CounterProvider>(context, listen: false);
        return Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Selector<CounterProvider, int>(
                  builder: (context, data, child) {
                    print('Update UI Consumer 1');
                    return Container(
                      height: 40,
                      color: Colors.red,
                      child: Center(child: Text('Consumer1: $data')),
                    );
                  },
                  selector: (buildContext, countPro) {
                    return countPro.getCount;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                Selector<CounterProvider, int>(
                  builder: (context, data, child) {
                    print('Update UI Consumer 2');
                    return Container(
                      height: 40,
                      color: Colors.blue,
                      child: Center(child: Text('Consumer2: $data')),
                    );
                  },
                  selector: (buildContext, counterProvider) =>
                      counterProvider.getCount1,
                ),
              ],
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton.extended(
                backgroundColor: Colors.red,
                onPressed: () {
                  counterProvider.incrementCounter();
                },
                label: Text('Consumer 1'),
                icon: Icon(Icons.add),
              ),
              Expanded(child: SizedBox()),
              FloatingActionButton.extended(
                backgroundColor: Colors.blue,
                onPressed: () {
                  counterProvider.incrementCounter1();
                },
                label: Text('Consumer 2'),
                icon: Icon(Icons.minimize),
              ),
            ],
          ),
        );
      },
    );
  }
}

class CounterProvider extends ChangeNotifier {
  int count = 0;
  int count1 = 0;

  int get getCount1 => count1;

  int get getCount => count;

  incrementCounter() {
    count++;
    notifyListeners();
  }

  incrementCounter1() {
    count1--;
    notifyListeners();
  }
}
