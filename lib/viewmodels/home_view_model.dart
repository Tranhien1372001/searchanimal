import 'package:animalsapp/data/remote/entity/animals_entity.dart';
import 'package:animalsapp/data/remote/repository/animal_repository.dart';
import 'package:animalsapp/view/screens/information.dart';
import 'package:flutter/material.dart';

class HomeViewModel extends ChangeNotifier {
  static List<AnimalEntity> _animalList = [];
  List<AnimalEntity> get animalList => List.from(_animalList);

  void onInit() {
    onRefresh();
  }

  void onRefresh() {
    _animalList = [];
    notifyListeners();
  }

  void navigatorToDeatail(BuildContext context, AnimalEntity data) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AnimalInformationView(animalEntity: data)),
    );
  }

  void loadDataTemplate() {
    _animalList = [];
    AnimalEntity a = AnimalEntity(
        name: "Cheetah",
        taxonomy: Taxonomy(
          kingdom: "Animalia",
          phylum: "Chordata",
          classic: "Mammalia",
          order: "Carnivora",
          family: "Felidae",
          genus: "Acinonyx",
          scientificName: "Acinonyx jubatus",
        ),
        locations: ["Africa", "Asia", "Eurasia"],
        characteristics: Characteristics(
          prey: "Gazelle, Wildebeest, Hare",
          nameOfYoung: "Cub",
          groupBehavior: "Solitary/Pairs",
          estimatedPopulationSize: "8,500",
          biggestThreat: "Habitat loss",
          mostDistinctiveFeature: "Yellowish fur covered in small black spots",
          gestationPeriod: "90 days",
          habitat: "Open grassland",
          diet: "Carnivore",
          averageLitterSize: "3",
          lifestyle: "Diurnal",
          commonName: "Cheetah",
          numberOfSpecies: "5",
          location: "Asia and Africa",
          slogan: "The fastest land mammal in the world!",
          weight: "40kg - 65kg (88lbs - 140lbs)",
          height: "115cm - 136cm (45in - 53in)",
          color: "BrownYellowBlackTan",
          skinType: "Fur",
          topSpeed: "70 mph",
          lifespan: "10 - 12 years",
          group: "Mammal",
          ageOfSexualMaturity: "20 - 24 months",
          ageOfWeaning: "3 months",
        ));
    _animalList.add(a);
    notifyListeners();
  }

  Future<void> search(String data) async {
    if (data == '') {
      onRefresh();
      notifyListeners();
      return;
    }
    _animalList = [];

    try {
      List<AnimalEntity> animal =
          await AnimalRepository.instance.searchanimals(search: data.trim());
      if (animal != null && animal.length > 0) {
        _animalList = animal;
        notifyListeners();
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
